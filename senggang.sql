-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 31, 2020 at 03:24 AM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `senggang`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_logs`
--

CREATE TABLE `activity_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `activity` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `users_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `activity_logs`
--

INSERT INTO `activity_logs` (`id`, `activity`, `created_at`, `updated_at`, `users_id`) VALUES
(1, 'ID of 1 category added.', '2020-01-29 16:38:37', '2020-01-29 16:38:37', 1),
(2, 'ID of 2 category added.', '2020-01-29 16:38:51', '2020-01-29 16:38:51', 1),
(3, 'ID of 1 product added.', '2020-01-29 16:40:50', '2020-01-29 16:40:50', 1),
(4, 'ID of 1 brand added.', '2020-01-29 16:42:26', '2020-01-29 16:42:26', 1),
(5, 'Id of 1 product status updated.', '2020-01-29 17:19:06', '2020-01-29 17:19:06', 1),
(6, 'ID of 1 address added.', '2020-01-29 17:19:33', '2020-01-29 17:19:33', 1),
(7, 'ID of 1 shipping added.', '2020-01-29 17:20:14', '2020-01-29 17:20:14', 1),
(8, 'ID of 1 address updated.', '2020-01-30 13:19:21', '2020-01-30 13:19:21', 1),
(9, 'ID of 1 address deleted.', '2020-01-30 13:20:09', '2020-01-30 13:20:09', 1),
(10, 'ID of 2 address added.', '2020-01-30 13:20:30', '2020-01-30 13:20:30', 1),
(11, 'ID of 3 address added.', '2020-01-30 13:25:42', '2020-01-30 13:25:42', 1),
(12, 'ID of 2 address deleted.', '2020-01-30 13:26:27', '2020-01-30 13:26:27', 1),
(13, 'ID of 1 user profile updated.', '2020-01-30 15:26:55', '2020-01-30 15:26:55', 1),
(14, 'ID of 2 product added.', '2020-01-30 16:50:31', '2020-01-30 16:50:31', 1),
(15, 'ID of 2 product update.', '2020-01-30 16:55:39', '2020-01-30 16:55:39', 1),
(16, 'ID of 1 product update.', '2020-01-30 17:42:11', '2020-01-30 17:42:11', 1),
(17, 'ID of 1 brand update.', '2020-01-30 17:52:24', '2020-01-30 17:52:24', 1),
(18, 'ID of 1 coupon added.', '2020-01-31 02:17:46', '2020-01-31 02:17:46', 1);

-- --------------------------------------------------------

--
-- Table structure for table `addresses`
--

CREATE TABLE `addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `users_id` int(10) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED DEFAULT NULL,
  `address_line_1` varchar(255) DEFAULT NULL,
  `address_line_2` varchar(255) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(45) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `pin_code` varchar(45) DEFAULT NULL,
  `primary` tinyint(3) UNSIGNED DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `countries__id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `addresses`
--

INSERT INTO `addresses` (`id`, `users_id`, `type`, `address_line_1`, `address_line_2`, `city`, `state`, `country`, `pin_code`, `primary`, `created_at`, `updated_at`, `status`, `countries__id`) VALUES
(1, 1, 1, 'Jl. Anggrek No.12, Perumnas', 'Maumere', 'Kota Kupang', 'Nusa Tenggara Timur', 'ID', '85858', 0, '2020-01-29 17:19:33', '2020-01-30 13:20:30', 3, 100),
(2, 1, 1, 'Jl. Anggrek No.12, Perumnas, Maumere', 'Jl. Anggrek No.12, Perumnas, Maumere', 'Kota Kupang', 'Nusa Tenggara Timur', 'ID', '85858', 0, '2020-01-30 13:20:30', '2020-01-30 13:26:27', 3, 100),
(3, 1, 1, 'Jln. Damai Oebufu, Kantor Transvision; Jln. Ade Irma no. 16 depan rumah jabatan walikota Kupang', 'Jln. Damai Oebufu, Kantor Transvision; Jln. Ade Irma no. 16 depan rumah jabatan walikota Kupang', 'Kupang', 'Nusa Tenggara Timur', 'ID', '85111', 1, '2020-01-30 13:25:42', '2020-01-30 13:25:47', 1, 100);

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `logo` varchar(150) DEFAULT NULL,
  `status` tinyint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`_id`, `name`, `created_at`, `updated_at`, `description`, `logo`, `status`) VALUES
(1, 'Puma', '2020-01-29 16:42:26', '2020-01-30 17:52:24', 'dyfasdfasy', 'images_5e3317d12403a.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `parent_id` smallint(5) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `status`, `created_at`, `updated_at`, `parent_id`) VALUES
(1, 'Elektronik', 1, '2020-01-29 16:38:37', '2020-01-29 16:38:37', NULL),
(2, 'TV', 1, '2020-01-29 16:38:51', '2020-01-29 16:38:51', 1);

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `_id` int(10) UNSIGNED NOT NULL,
  `iso_code` char(2) NOT NULL,
  `name_capitalized` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `iso3_code` char(3) DEFAULT NULL,
  `iso_num_code` smallint(6) DEFAULT NULL,
  `phone_code` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`_id`, `iso_code`, `name_capitalized`, `name`, `iso3_code`, `iso_num_code`, `phone_code`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, 93),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, 355),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, 213),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, 1684),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, 376),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, 244),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, 1264),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, 0),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, 1268),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, 54),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, 374),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, 297),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, 61),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, 43),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, 994),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, 1242),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, 973),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, 880),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, 1246),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, 375),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, 32),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, 501),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, 229),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, 1441),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, 975),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, 591),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, 387),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, 267),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, 0),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, 55),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, 246),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, 673),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, 359),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, 226),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, 257),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, 855),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, 237),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, 1),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, 238),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, 1345),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, 236),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, 235),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, 56),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, 86),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, 61),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, 672),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, 57),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, 269),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, 242),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, 243),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, 682),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, 506),
(53, 'CI', 'COTE D\'IVOIRE', 'Cote D\'Ivoire', 'CIV', 384, 225),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, 385),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, 53),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, 357),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, 420),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, 45),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, 253),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, 1767),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, 1809),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, 593),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, 20),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, 503),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, 240),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, 291),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, 372),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, 251),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, 500),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, 298),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, 679),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, 358),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, 33),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, 594),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, 689),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, 0),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, 241),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, 220),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, 995),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, 49),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, 233),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, 350),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, 30),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, 299),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, 1473),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, 590),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, 1671),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, 502),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, 224),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, 245),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, 592),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, 509),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, 0),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, 39),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, 504),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, 852),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, 36),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, 354),
(99, 'IN', 'INDIA', 'India', 'IND', 356, 91),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, 62),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, 98),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, 964),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, 353),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, 972),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, 39),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, 1876),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, 81),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, 962),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, 7),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, 254),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, 686),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF', 'Korea, Democratic People\'s Republic of', 'PRK', 408, 850),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, 82),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, 965),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, 996),
(116, 'LA', 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC', 'Lao People\'s Democratic Republic', 'LAO', 418, 856),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, 371),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, 961),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, 266),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, 231),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, 218),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, 423),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, 370),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, 352),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, 853),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, 389),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, 261),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, 265),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, 60),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, 960),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, 223),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, 356),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, 692),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, 596),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, 222),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, 230),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, 269),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, 52),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, 691),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, 373),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, 377),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, 976),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, 1664),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, 212),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, 258),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, 95),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, 264),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, 674),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, 977),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, 31),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, 599),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, 687),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, 64),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, 505),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, 227),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, 234),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, 683),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, 672),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, 1670),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, 47),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, 968),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, 92),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, 680),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, 970),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, 507),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, 675),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, 595),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, 51),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, 63),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, 0),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, 48),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, 351),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, 1787),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, 974),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, 262),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, 40),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, 7),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, 250),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, 290),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, 1869),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, 1758),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, 508),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, 1784),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, 684),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, 378),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, 239),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, 966),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, 221),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, 248),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, 232),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, 65),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, 421),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, 386),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, 677),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, 252),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, 27),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, 0),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, 34),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, 94),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, 249),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, 597),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, 47),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, 268),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, 46),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, 41),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, 963),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, 886),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, 992),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, 255),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, 66),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, 670),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, 228),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, 690),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, 676),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, 1868),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, 216),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, 90),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, 7370),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, 1649),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, 688),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, 256),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, 380),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, 971),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, 44),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, 1),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, 1),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, 598),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, 998),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, 678),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, 58),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, 84),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, 1284),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, 1340),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, 681),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, 212),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, 967),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, 260),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, 263),
(240, 'RS', 'SERBIA', 'Serbia', 'SRB', 688, 381),
(241, 'AP', 'ASIA PACIFIC REGION', 'Asia / Pacific Region', '0', 0, 0),
(242, 'ME', 'MONTENEGRO', 'Montenegro', 'MNE', 499, 382),
(243, 'AX', 'ALAND ISLANDS', 'Aland Islands', 'ALA', 248, 358),
(244, 'BQ', 'BONAIRE, SINT EUSTATIUS AND SABA', 'Bonaire, Sint Eustatius and Saba', 'BES', 535, 599),
(245, 'CW', 'CURACAO', 'Curacao', 'CUW', 531, 599),
(246, 'GG', 'GUERNSEY', 'Guernsey', 'GGY', 831, 44),
(247, 'IM', 'ISLE OF MAN', 'Isle of Man', 'IMN', 833, 44),
(248, 'JE', 'JERSEY', 'Jersey', 'JEY', 832, 44),
(249, 'XK', 'KOSOVO', 'Kosovo', '---', 0, 381),
(250, 'BL', 'SAINT BARTHELEMY', 'Saint Barthelemy', 'BLM', 652, 590),
(251, 'MF', 'SAINT MARTIN', 'Saint Martin', 'MAF', 663, 590),
(252, 'SX', 'SINT MAARTEN', 'Sint Maarten', 'SXM', 534, 1),
(253, 'SS', 'SOUTH SUDAN', 'South Sudan', 'SSD', 728, 211),
(999, '__', 'AOC - ALL OTHER COUNTRIES', 'AOC - All Other Countries', 'AOC', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(80) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `code` varchar(45) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `discount` double UNSIGNED DEFAULT NULL,
  `discount_type` tinyint(3) UNSIGNED DEFAULT NULL,
  `max_discount` double DEFAULT NULL,
  `status` tinyint(3) UNSIGNED DEFAULT NULL,
  `minimum_order_amount` double DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `uses_per_user` smallint(6) DEFAULT NULL,
  `products_scope` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `coupons`
--

INSERT INTO `coupons` (`_id`, `title`, `description`, `code`, `start`, `end`, `discount`, `discount_type`, `max_discount`, `status`, `minimum_order_amount`, `created_at`, `updated_at`, `uses_per_user`, `products_scope`) VALUES
(1, 'asas', NULL, 'RL3JN', '2020-01-31 02:16:25', '2021-01-27 02:16:25', 12, 1, 12, 1, 1, '2020-01-31 02:17:46', '2020-01-31 02:17:46', 9, 1);

-- --------------------------------------------------------

--
-- Table structure for table `coupon_products`
--

CREATE TABLE `coupon_products` (
  `_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `coupons__id` int(10) UNSIGNED NOT NULL,
  `products_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `created_at` datetime NOT NULL,
  `attempts` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ordered_products`
--

CREATE TABLE `ordered_products` (
  `_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `products_id` int(10) UNSIGNED DEFAULT NULL,
  `orders__id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` double DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `quantity` smallint(6) DEFAULT NULL,
  `custom_product_id` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ordered_products`
--

INSERT INTO `ordered_products` (`_id`, `created_at`, `updated_at`, `products_id`, `orders__id`, `name`, `price`, `status`, `quantity`, `custom_product_id`) VALUES
(1, '2020-01-29 17:21:04', '2020-01-29 17:21:04', 1, 1, 'Samsung', 14, 1, 3, '1234'),
(2, '2020-01-30 13:27:39', '2020-01-30 13:27:39', 1, 2, 'Samsung', 14, 1, 1, '1234'),
(3, '2020-01-30 14:54:48', '2020-01-30 14:54:48', 1, 3, 'Samsung', 14, 1, 1, '1234');

-- --------------------------------------------------------

--
-- Table structure for table `ordered_product_options`
--

CREATE TABLE `ordered_product_options` (
  `_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `ordered_products__id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `value_name` varchar(255) DEFAULT NULL,
  `addon_price` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `_id` int(10) UNSIGNED NOT NULL,
  `order_uid` varchar(16) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `status` tinyint(3) UNSIGNED DEFAULT NULL,
  `type` tinyint(3) UNSIGNED DEFAULT NULL,
  `payment_method` tinyint(3) UNSIGNED DEFAULT NULL,
  `payment_status` tinyint(3) UNSIGNED DEFAULT NULL,
  `users_id` int(10) UNSIGNED NOT NULL,
  `addresses_id` int(10) UNSIGNED NOT NULL,
  `addresses_id1` int(10) UNSIGNED NOT NULL,
  `currency_code` varchar(5) DEFAULT NULL,
  `name` varchar(80) DEFAULT NULL,
  `coupons__id` int(10) UNSIGNED DEFAULT NULL,
  `discount_amount` double DEFAULT NULL,
  `shipping_amount` double DEFAULT NULL,
  `total_amount` double DEFAULT NULL,
  `business_email` varchar(150) DEFAULT NULL,
  `__data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`_id`, `order_uid`, `created_at`, `updated_at`, `status`, `type`, `payment_method`, `payment_status`, `users_id`, `addresses_id`, `addresses_id1`, `currency_code`, `name`, `coupons__id`, `discount_amount`, `shipping_amount`, `total_amount`, `business_email`, `__data`) VALUES
(1, '5e31bf009755f', '2020-01-29 17:21:04', '2020-01-30 10:36:41', 2, 1, 3, 2, 1, 1, 1, 'Rp', 'Store Administrator', NULL, NULL, 12121, 12163, 'support@senggang.id', '{\"shipping_method_id\":1,\"shippingsData\":[{\"_id\":1,\"country\":\"ID\",\"type\":1,\"charges\":12121,\"free_after_amount\":121121212,\"amount_cap\":null,\"status\":1,\"notes\":\"\",\"created_at\":\"2020-01-29 17:20:14\",\"updated_at\":\"2020-01-29 17:20:14\",\"countries__id\":100,\"__data\":null,\"shipping_types__id\":1,\"shipping_method_title\":\"JNE\",\"base_shipping_method_title\":\"JNE\"}]}'),
(2, '5e32d9cbb8ee7', '2020-01-30 13:27:39', '2020-01-30 13:27:39', 1, 1, 3, 1, 1, 3, 3, 'Rp', 'Store Administrator', NULL, NULL, 12121, 12135, 'support@senggang.id', '{\"shipping_method_id\":1,\"shippingsData\":[{\"_id\":1,\"country\":\"ID\",\"type\":1,\"charges\":12121,\"free_after_amount\":121121212,\"amount_cap\":null,\"status\":1,\"notes\":\"\",\"created_at\":\"2020-01-29 17:20:14\",\"updated_at\":\"2020-01-29 17:20:14\",\"countries__id\":100,\"__data\":null,\"shipping_types__id\":1,\"shipping_method_title\":\"JNE\",\"base_shipping_method_title\":\"JNE\"}]}'),
(3, '5e32ee3817c33', '2020-01-30 14:54:48', '2020-01-30 14:54:48', 1, 1, 3, 1, 1, 3, 3, 'Rp', 'Store Administrator', NULL, NULL, 12121, 12135, 'support@senggang.id', '{\"shipping_method_id\":1,\"shippingsData\":[{\"_id\":1,\"country\":\"ID\",\"type\":1,\"charges\":12121,\"free_after_amount\":121121212,\"amount_cap\":null,\"status\":1,\"notes\":\"\",\"created_at\":\"2020-01-29 17:20:14\",\"updated_at\":\"2020-01-29 17:20:14\",\"countries__id\":100,\"__data\":null,\"shipping_types__id\":1,\"shipping_method_title\":\"JNE\",\"base_shipping_method_title\":\"JNE\"}]}');

-- --------------------------------------------------------

--
-- Table structure for table `order_logs`
--

CREATE TABLE `order_logs` (
  `_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `orders__id` int(10) UNSIGNED NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `users_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_logs`
--

INSERT INTO `order_logs` (`_id`, `created_at`, `updated_at`, `orders__id`, `description`, `users_id`, `ip_address`) VALUES
(1, '2020-01-29 17:21:04', '2020-01-29 17:21:04', 1, '{\"createdAt\":\"On Wednesday 29th January 2020  5:21pm by Store Administrator\",\"message\":\"Pesanan Baru\"}', 1, '::1'),
(2, '2020-01-30 10:36:08', '2020-01-30 10:36:08', 1, '{\"createdAt\":\"On Thursday 30th January 2020  10:36am by Store Administrator\",\"message\":\"Order status updated from New to Processing\"}', 1, '::1'),
(3, '2020-01-30 10:36:41', '2020-01-30 10:36:41', 1, '{\"createdAt\":\"On Thursday 30th January 2020  10:36am by Store Administrator\",\"message\":\"Payment for Order 1 has been recorded.\"}', 1, '::1'),
(4, '2020-01-30 13:27:39', '2020-01-30 13:27:39', 2, '{\"createdAt\":\"On Thursday 30th January 2020  1:27pm by Store Administrator\",\"message\":\"New Order Submitted\"}', 1, '::1'),
(5, '2020-01-30 14:54:48', '2020-01-30 14:54:48', 3, '{\"createdAt\":\"On Thursday 30th January 2020  2:54pm by Store Administrator\",\"message\":\"New Order Submitted\"}', 1, '::1');

-- --------------------------------------------------------

--
-- Table structure for table `order_payments`
--

CREATE TABLE `order_payments` (
  `_id` int(10) UNSIGNED NOT NULL,
  `txn` varchar(45) DEFAULT NULL,
  `type` tinyint(3) UNSIGNED DEFAULT NULL,
  `payment_method` tinyint(3) UNSIGNED DEFAULT NULL,
  `currency_code` varchar(5) DEFAULT NULL,
  `gross_amount` double DEFAULT NULL,
  `fee` double DEFAULT NULL,
  `orders__id` int(10) UNSIGNED NOT NULL,
  `raw_data` text,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `order_payments`
--

INSERT INTO `order_payments` (`_id`, `txn`, `type`, `payment_method`, `currency_code`, `gross_amount`, `fee`, `orders__id`, `raw_data`, `created_at`, `updated_at`) VALUES
(1, 'ADS1212', 1, 3, 'Rp', 12163, NULL, 1, '{\"comment\":\"sukses\"}', '2020-01-30 10:36:41', '2020-01-30 10:36:41');

-- --------------------------------------------------------

--
-- Table structure for table `order_taxes`
--

CREATE TABLE `order_taxes` (
  `_id` int(10) UNSIGNED NOT NULL,
  `tax__id` int(10) UNSIGNED DEFAULT NULL,
  `orders__id` int(10) UNSIGNED NOT NULL,
  `amount` double DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `type` tinyint(3) UNSIGNED NOT NULL,
  `description` text NOT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `add_to_menu` tinyint(4) DEFAULT NULL,
  `parent_id` tinyint(4) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `link_details` varchar(255) DEFAULT NULL,
  `list_order` smallint(6) DEFAULT NULL,
  `hide_sidebar` tinyint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `type`, `description`, `status`, `add_to_menu`, `parent_id`, `created_at`, `updated_at`, `link_details`, `list_order`, `hide_sidebar`) VALUES
(1, 'Home', 1, 'Your home page content', 1, 1, NULL, '2020-01-29 16:35:39', '2020-01-29 16:35:39', 'home', 1, NULL),
(2, 'Categories', 3, '', 1, 1, NULL, '2020-01-29 16:35:39', '2020-01-29 16:35:39', 'products', 2, NULL),
(3, 'Brands', 3, '', 1, 1, NULL, '2020-01-29 16:35:39', '2020-01-29 16:35:39', 'fetch.brands', 3, NULL),
(4, 'Login', 3, '', 1, 1, NULL, '2020-01-29 16:35:39', '2020-01-29 16:35:39', 'user.login', 6, NULL),
(5, 'Register', 3, '', 1, 1, NULL, '2020-01-29 16:35:39', '2020-01-29 16:35:39', 'user.register', 7, NULL),
(6, 'Contact', 3, '', 1, 1, NULL, '2020-01-29 16:35:39', '2020-01-29 16:35:39', 'get.user.contact', 4, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `product_id` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `price` double NOT NULL,
  `old_price` double DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `featured` tinyint(4) DEFAULT NULL,
  `out_of_stock` tinyint(4) DEFAULT NULL,
  `brands__id` int(10) UNSIGNED DEFAULT NULL,
  `youtube_video` varchar(255) DEFAULT NULL,
  `specification_presets__id` int(10) UNSIGNED DEFAULT NULL,
  `__data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `thumbnail`, `product_id`, `description`, `price`, `old_price`, `status`, `created_at`, `updated_at`, `featured`, `out_of_stock`, `brands__id`, `youtube_video`, `specification_presets__id`, `__data`) VALUES
(1, 'Samsung', 'logo-betablanja_5e31b55e287a4.png', '1234', 'lorem ipsum', 14, 12, 1, '2020-01-29 16:40:50', '2020-01-30 17:42:11', 0, 0, 1, NULL, NULL, '[]'),
(2, 'Ralph Waworuntu', 'logo-betablanja_5e31b56c45972.png', 'qqweq23', '121212121212121', 1212121, NULL, 1, '2020-01-30 16:50:31', '2020-01-30 16:55:39', 1, 0, 1, NULL, NULL, '[]');

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `categories_id` smallint(5) UNSIGNED NOT NULL,
  `products_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `categories_id`, `products_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2020-01-29 16:40:50', '2020-01-29 16:40:50'),
(2, 2, 2, '2020-01-30 16:50:31', '2020-01-30 16:50:31');

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `products_id` int(10) UNSIGNED NOT NULL,
  `list_order` tinyint(3) UNSIGNED DEFAULT NULL,
  `product_option_values_id` mediumint(8) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_notifications`
--

CREATE TABLE `product_notifications` (
  `_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL,
  `email` varchar(255) NOT NULL,
  `products_id` int(10) UNSIGNED NOT NULL,
  `users_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_option_labels`
--

CREATE TABLE `product_option_labels` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `products_id` int(10) UNSIGNED NOT NULL,
  `type` tinyint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_option_values`
--

CREATE TABLE `product_option_values` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `addon_price` double DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `product_option_labels_id` smallint(5) UNSIGNED NOT NULL,
  `image_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_questions`
--

CREATE TABLE `product_questions` (
  `_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL,
  `products_id` int(10) UNSIGNED DEFAULT NULL,
  `question` varchar(255) NOT NULL,
  `answer` text,
  `users_id` int(10) UNSIGNED DEFAULT NULL,
  `type` tinyint(3) UNSIGNED DEFAULT NULL,
  `product_questions__id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_ratings`
--

CREATE TABLE `product_ratings` (
  `_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `rating` tinyint(3) UNSIGNED DEFAULT NULL,
  `review` text,
  `users_id` int(10) UNSIGNED DEFAULT NULL,
  `products_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `product_specifications`
--

CREATE TABLE `product_specifications` (
  `_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `products_id` int(10) UNSIGNED NOT NULL,
  `specifications__id` int(10) UNSIGNED DEFAULT NULL,
  `specification_values__id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `related_products`
--

CREATE TABLE `related_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `products_id` int(10) UNSIGNED NOT NULL,
  `related_product_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `related_products`
--

INSERT INTO `related_products` (`id`, `created_at`, `updated_at`, `products_id`, `related_product_id`) VALUES
(1, '2020-01-30 16:50:31', '2020-01-30 16:50:31', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `name` varchar(150) NOT NULL,
  `value` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`, `created_at`, `updated_at`) VALUES
(1, 'store_name', 'SENGGANG', '2020-01-29 16:38:11', '2020-01-29 16:38:11'),
(2, 'logo_background_color', '005bb9', '2020-01-29 16:38:11', '2020-01-29 16:38:11'),
(3, 'timezone', 'UTC', '2020-01-29 16:38:11', '2020-01-29 16:38:11'),
(4, 'business_email', 'support@senggang.id', '2020-01-29 16:38:11', '2020-01-29 16:38:11'),
(5, 'show_language_menu', '1', '2020-01-29 16:38:11', '2020-01-29 16:38:11'),
(6, 'default_language', 'en_US', '2020-01-29 16:38:11', '2020-01-29 16:38:11'),
(7, 'currency_symbol', '&#36;', '2020-01-29 16:54:47', '2020-01-29 16:54:47'),
(8, 'currency', 'other', '2020-01-29 16:54:47', '2020-01-29 16:54:47'),
(9, 'currency_value', 'Rp', '2020-01-29 16:54:47', '2020-01-29 16:54:47'),
(10, 'currency_format', '{__currencyCode__} {__amount__}', '2020-01-29 16:54:47', '2020-01-29 16:54:47'),
(11, 'round_zero_decimal_currency', '1', '2020-01-29 16:54:47', '2020-01-29 16:54:47'),
(12, 'display_multi_currency', '0', '2020-01-29 16:54:47', '2020-01-29 16:54:47'),
(13, 'auto_refresh_in', '1', '2020-01-29 16:54:47', '2020-01-29 16:54:47'),
(14, 'show_out_of_stock', '1', '2020-01-29 17:00:28', '2020-01-29 17:00:28'),
(15, 'pagination_count', '20', '2020-01-29 17:00:28', '2020-01-29 17:00:28'),
(16, 'item_load_type', '2', '2020-01-29 17:00:28', '2020-01-29 17:00:28'),
(17, 'enable_rating', '1', '2020-01-29 17:00:28', '2020-01-29 17:00:28'),
(18, 'enable_rating_review', '1', '2020-01-29 17:00:28', '2020-01-29 17:00:28'),
(19, 'restrict_add_rating_to_item_purchased_users', '1', '2020-01-29 17:00:28', '2020-01-29 17:00:28'),
(20, 'enable_rating_modification', '0', '2020-01-29 17:00:28', '2020-01-29 17:00:28'),
(21, 'enable_user_add_questions', '0', '2020-01-29 17:00:28', '2020-01-29 17:00:28'),
(22, 'facebook', '1', '2020-01-29 17:00:28', '2020-01-29 17:00:28'),
(23, 'twitter', '1', '2020-01-29 17:00:28', '2020-01-29 17:00:28'),
(24, 'enable_whatsapp', '1', '2020-01-29 17:00:28', '2020-01-29 17:00:28'),
(25, 'enable_staticaly_cdn', '0', '2020-01-29 17:00:28', '2020-01-29 17:00:28'),
(26, 'select_payment_option', '[3,4,2,5]', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(27, 'payment_other_text', 'Add here other payment related information', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(28, 'payment_check_text', 'Add here check related information.', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(29, 'payment_bank_text', 'Add here bank related information', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(30, 'payment_cod_text', 'Add here cod related information.', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(31, 'payment_other', '0', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(32, 'use_paypal', '0', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(33, 'paypal_email', '', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(34, 'paypal_sandbox_email', '', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(35, 'use_stripe', '1', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(36, 'stripe_live_secret_key', '', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(37, 'stripe_live_publishable_key', '', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(38, 'stripe_testing_secret_key', '', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(39, 'stripe_testing_publishable_key', '', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(40, 'use_razorpay', '0', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(41, 'razorpay_live_key', '', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(42, 'razorpay_live_secret_key', '', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(43, 'razorpay_testing_key', '', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(44, 'razorpay_testing_secret_key', '', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(45, 'use_iyzipay', '0', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(46, 'iyzipay_live_key', '', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(47, 'iyzipay_live_secret_key', '', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(48, 'iyzipay_testing_key', '', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(49, 'iyzipay_testing_secret_key', '', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(50, 'use_paytm', '0', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(51, 'paytm_live_merchant_key', '', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(52, 'paytm_live_merchant_mid_key', '', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(53, 'paytm_testing_merchant_key', '', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(54, 'paytm_testing_merchant_mid_key', '', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(55, 'use_instamojo', '0', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(56, 'instamojo_live_api_key', '', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(57, 'instamojo_live_auth_token_key', '', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(58, 'instamojo_testing_api_key', '', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(59, 'instamojo_testing_auth_token_key', '', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(60, 'use_payStack', '0', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(61, 'payStack_testing_secret_key', '', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(62, 'payStack_testing_public_key', '', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(63, 'payStack_live_secret_key', '', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(64, 'payStack_live_public_key', '', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(65, 'payment_check', '0', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(66, 'payment_bank', '0', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(67, 'payment_cod', '0', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(68, 'use_iyzico', '0', '2020-01-29 17:20:47', '2020-01-29 17:20:47'),
(69, 'enable_guest_order', '0', '2020-01-29 17:21:23', '2020-01-29 17:21:23'),
(70, 'apply_tax_after_before_discount', '1', '2020-01-29 17:21:23', '2020-01-29 17:21:23'),
(71, 'calculate_tax_as_per_shipping_billing', '1', '2020-01-29 17:21:23', '2020-01-29 17:21:23'),
(72, 'allow_customer_order_cancellation', '0', '2020-01-29 17:21:23', '2020-01-29 17:21:23'),
(73, 'order_cancellation_statuses', '\"{}\"', '2020-01-29 17:21:23', '2020-01-29 17:21:23'),
(74, 'home_page', '1', '2020-01-30 15:30:36', '2020-01-30 15:30:36'),
(75, 'landing_page', '{\"landingPageData\":[{\"title\":null,\"orderIndex\":0,\"identity\":\"Slider\",\"isEnable\":true},{\"pageContent\":null,\"orderIndex\":1,\"identity\":\"PageContent\",\"isEnable\":true},{\"productCount\":10,\"orderIndex\":2,\"identity\":\"latestProduct\",\"isEnable\":true},{\"featuredProductCount\":10,\"orderIndex\":3,\"identity\":\"featuredProduct\",\"isEnable\":true},{\"popularProductCount\":10,\"orderIndex\":4,\"identity\":\"popularProduct\",\"isEnable\":true},{\"title\":\"3BoxBanner\",\"banner_1_section_1_image_thumb\":\"\",\"banner_1_section_1_image\":null,\"banner_1_section_1_heading_1\":null,\"banner_1_section_1_heading_1_color\":null,\"banner_1_section_1_heading_2\":null,\"banner_1_section_1_heading_2_color\":null,\"banner_1_section_1_description\":null,\"banner_1_section_1_background_color\":null,\"banner_1_section_2_image_thumb\":\"\",\"banner_1_section_2_image\":null,\"baner_1_section_2_heading_1\":null,\"baner_1_section_2_heading_1_color\":null,\"baner_1_section_2_heading_2\":null,\"baner_1_section_2_heading_2_color\":null,\"baner_1_section_2_description\":null,\"baner_1_section_2_background_color\":null,\"banner_1_section_3_image_thumb\":\"\",\"banner_1_section_3_image\":null,\"baner_1_section_3_heading_1\":null,\"baner_1_section_3_heading_1_color\":null,\"baner_1_section_3_heading_2\":null,\"baner_1_section_3_heading_2_color\":null,\"baner_1_section_3_description\":null,\"baner_1_section_3_background_color\":null,\"orderIndex\":5,\"identity\":\"bannerContent1\",\"isEnable\":false},{\"title\":\"2BoxBanner\",\"banner_2_section_1_image_thumb\":\"\",\"banner_2_section_1_image\":null,\"banner_2_section_1_heading_1\":null,\"banner_2_section_1_heading_1_color\":null,\"banner_2_section_1_heading_2\":null,\"banner_2_section_1_heading_2_color\":null,\"banner_2_section_1_description\":null,\"banner_2_section_1_background_color\":null,\"banner_2_section_2_image_thumb\":\"\",\"banner_2_section_2_image\":null,\"baner_2_section_2_heading_1\":null,\"baner_2_section_2_heading_1_color\":null,\"baner_2_section_2_heading_2\":null,\"baner_2_section_2_heading_2_color\":null,\"baner_2_section_2_description\":null,\"baner_2_section_2_background_color\":null,\"orderIndex\":6,\"identity\":\"bannerContent2\",\"isEnable\":false},{\"title\":\"productTabSection\",\"tab_section_title\":null,\"tab_1_title\":null,\"tab_1_products\":[],\"tab_2_title\":null,\"tab_2_products\":[],\"tab_3_title\":null,\"tab_3_products\":[],\"tab_4_title\":null,\"tab_4_products\":[],\"orderIndex\":7,\"identity\":\"productTabContent\",\"isEnable\":false}],\"home_page\":\"1\"}', '2020-01-30 15:30:36', '2020-01-30 15:30:36');

-- --------------------------------------------------------

--
-- Table structure for table `shipping`
--

CREATE TABLE `shipping` (
  `_id` int(10) UNSIGNED NOT NULL,
  `country` varchar(10) DEFAULT NULL,
  `type` tinyint(3) UNSIGNED DEFAULT NULL,
  `charges` double DEFAULT NULL,
  `free_after_amount` double DEFAULT NULL,
  `amount_cap` double DEFAULT NULL,
  `status` tinyint(3) UNSIGNED DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `countries__id` int(10) UNSIGNED DEFAULT NULL,
  `__data` varchar(45) DEFAULT NULL,
  `shipping_types__id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shipping`
--

INSERT INTO `shipping` (`_id`, `country`, `type`, `charges`, `free_after_amount`, `amount_cap`, `status`, `notes`, `created_at`, `updated_at`, `countries__id`, `__data`, `shipping_types__id`) VALUES
(1, 'ID', 1, 12121, 121121212, NULL, 1, '', '2020-01-29 17:20:14', '2020-01-29 17:20:14', 100, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `shipping_types`
--

CREATE TABLE `shipping_types` (
  `_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL,
  `title` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shipping_types`
--

INSERT INTO `shipping_types` (`_id`, `created_at`, `updated_at`, `status`, `title`) VALUES
(1, '2020-01-29 17:19:53', '2020-01-29 17:19:53', 1, 'JNE');

-- --------------------------------------------------------

--
-- Table structure for table `social_user_registrations`
--

CREATE TABLE `social_user_registrations` (
  `_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `provider` tinyint(3) UNSIGNED NOT NULL,
  `account_id` varchar(100) DEFAULT NULL,
  `users_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `specifications`
--

CREATE TABLE `specifications` (
  `_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL,
  `label` varchar(150) NOT NULL,
  `use_for_filter` tinyint(3) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `specification_presets`
--

CREATE TABLE `specification_presets` (
  `_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL,
  `title` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `specification_preset_items`
--

CREATE TABLE `specification_preset_items` (
  `_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `specification_presets__id` int(10) UNSIGNED NOT NULL,
  `specifications__id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `specification_values`
--

CREATE TABLE `specification_values` (
  `_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL,
  `specification_value` varchar(255) NOT NULL,
  `specifications__id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tax`
--

CREATE TABLE `tax` (
  `_id` int(10) UNSIGNED NOT NULL,
  `label` varchar(45) DEFAULT NULL,
  `country` varchar(10) DEFAULT NULL,
  `applicable_tax` double DEFAULT NULL,
  `type` tinyint(3) UNSIGNED DEFAULT NULL,
  `status` tinyint(3) UNSIGNED DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `countries__id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `temp_emails`
--

CREATE TABLE `temp_emails` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `new_email` varchar(150) NOT NULL,
  `activation_key` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `users_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `transliterates`
--

CREATE TABLE `transliterates` (
  `_id` varchar(64) NOT NULL,
  `string` text NOT NULL,
  `language` varchar(10) NOT NULL,
  `entity_type` varchar(85) NOT NULL,
  `entity_id` int(11) DEFAULT NULL,
  `entity_key` varchar(85) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` tinyint(4) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `fname` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  `email` varchar(150) NOT NULL,
  `banned` tinyint(4) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `last_ip` varchar(45) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `__permissions` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `password`, `role`, `status`, `fname`, `lname`, `email`, `banned`, `created_at`, `updated_at`, `remember_token`, `last_ip`, `last_login`, `__permissions`) VALUES
(1, '$2y$10$zBcNfttqbN2QuYGSH8m/Puj0KiYXAqNvsIVIy4nCDQUbcaxl.ALm6', 1, 1, 'Sempak', 'Firaun', 'firstadmin@domain.com', 0, '2020-01-29 16:35:39', '2020-01-31 02:16:21', 'N017FpKddj6OP0rS6XfSCDzPLnLfPcBRrvARof7tYDjZ0eZq4B6hKJZnBqGg', '::1', '2020-01-31 02:16:21', '[]');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `_id` tinyint(3) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL,
  `__permissions` text,
  `title` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`_id`, `created_at`, `updated_at`, `status`, `__permissions`, `title`) VALUES
(1, '2020-01-29 16:35:39', '2020-01-29 16:35:39', 1, NULL, 'Admin'),
(2, '2020-01-29 16:35:39', '2020-01-29 16:35:39', 1, NULL, 'Customer');

-- --------------------------------------------------------

--
-- Table structure for table `wishlist`
--

CREATE TABLE `wishlist` (
  `_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `products_id` int(10) UNSIGNED NOT NULL,
  `users_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_logs`
--
ALTER TABLE `activity_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `addresses`
--
ALTER TABLE `addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_addresses_users1_idx` (`users_id`),
  ADD KEY `fk_addresses_countries1_idx` (`countries__id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `coupon_products`
--
ALTER TABLE `coupon_products`
  ADD PRIMARY KEY (`_id`),
  ADD KEY `fk_coupon_products_coupons1_idx` (`coupons__id`),
  ADD KEY `fk_coupon_products_products1_idx` (`products_id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ordered_products`
--
ALTER TABLE `ordered_products`
  ADD PRIMARY KEY (`_id`),
  ADD KEY `fk_ordered_producs_products1_idx` (`products_id`),
  ADD KEY `fk_ordered_producs_orders1_idx` (`orders__id`);

--
-- Indexes for table `ordered_product_options`
--
ALTER TABLE `ordered_product_options`
  ADD PRIMARY KEY (`_id`),
  ADD KEY `fk_ordered_product_options_ordered_products1_idx` (`ordered_products__id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`_id`),
  ADD KEY `fk_orders_users1_idx` (`users_id`),
  ADD KEY `fk_orders_addresses1_idx` (`addresses_id`),
  ADD KEY `fk_orders_addresses2_idx` (`addresses_id1`),
  ADD KEY `fk_orders_coupons1_idx` (`coupons__id`);

--
-- Indexes for table `order_logs`
--
ALTER TABLE `order_logs`
  ADD PRIMARY KEY (`_id`),
  ADD KEY `fk_order_logs_orders1_idx` (`orders__id`),
  ADD KEY `fk_order_logs_users1_idx` (`users_id`);

--
-- Indexes for table `order_payments`
--
ALTER TABLE `order_payments`
  ADD PRIMARY KEY (`_id`),
  ADD KEY `fk_order_payments_orders1_idx` (`orders__id`);

--
-- Indexes for table `order_taxes`
--
ALTER TABLE `order_taxes`
  ADD PRIMARY KEY (`_id`),
  ADD KEY `fk_order_taxes_tax1_idx` (`tax__id`),
  ADD KEY `fk_order_taxes_orders1_idx` (`orders__id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_products_brands1_idx` (`brands__id`),
  ADD KEY `fk_products_specification_presets1_idx` (`specification_presets__id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_product_catgeories_categories1_idx` (`categories_id`),
  ADD KEY `fk_product_catgeories_products1_idx` (`products_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_product_images_products1_idx` (`products_id`),
  ADD KEY `fk_product_images_product_option_values1_idx` (`product_option_values_id`);

--
-- Indexes for table `product_notifications`
--
ALTER TABLE `product_notifications`
  ADD PRIMARY KEY (`_id`),
  ADD KEY `fk_product_notifications_products1_idx` (`products_id`),
  ADD KEY `fk_product_notifications_users1_idx` (`users_id`);

--
-- Indexes for table `product_option_labels`
--
ALTER TABLE `product_option_labels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_product_option_labels_products1_idx` (`products_id`);

--
-- Indexes for table `product_option_values`
--
ALTER TABLE `product_option_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_product_option_values_product_option_labels1_idx` (`product_option_labels_id`);

--
-- Indexes for table `product_questions`
--
ALTER TABLE `product_questions`
  ADD PRIMARY KEY (`_id`),
  ADD KEY `fk_faqs_products1_idx` (`products_id`),
  ADD KEY `fk_faqs_users1_idx` (`users_id`),
  ADD KEY `fk_product_questions_product_questions1_idx` (`product_questions__id`);

--
-- Indexes for table `product_ratings`
--
ALTER TABLE `product_ratings`
  ADD PRIMARY KEY (`_id`),
  ADD KEY `fk_product_ratings_users1_idx` (`users_id`),
  ADD KEY `fk_product_ratings_products1_idx` (`products_id`);

--
-- Indexes for table `product_specifications`
--
ALTER TABLE `product_specifications`
  ADD PRIMARY KEY (`_id`),
  ADD KEY `fk_product_specifications_products1_idx` (`products_id`),
  ADD KEY `fk_product_specifications_specifications1_idx` (`specifications__id`),
  ADD KEY `fk_product_specifications_specification_values1_idx` (`specification_values__id`);

--
-- Indexes for table `related_products`
--
ALTER TABLE `related_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_related_products_products1_idx` (`products_id`),
  ADD KEY `fk_related_products_products2_idx` (`related_product_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`);

--
-- Indexes for table `shipping`
--
ALTER TABLE `shipping`
  ADD PRIMARY KEY (`_id`),
  ADD KEY `fk_shipping_countries1_idx` (`countries__id`),
  ADD KEY `fk_shipping_shipping_types1_idx` (`shipping_types__id`);

--
-- Indexes for table `shipping_types`
--
ALTER TABLE `shipping_types`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `social_user_registrations`
--
ALTER TABLE `social_user_registrations`
  ADD PRIMARY KEY (`_id`),
  ADD KEY `fk_social_user_registrations_users1_idx` (`users_id`);

--
-- Indexes for table `specifications`
--
ALTER TABLE `specifications`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `specification_presets`
--
ALTER TABLE `specification_presets`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `specification_preset_items`
--
ALTER TABLE `specification_preset_items`
  ADD PRIMARY KEY (`_id`),
  ADD KEY `fk_specification_preset_items_specification_presets1_idx` (`specification_presets__id`),
  ADD KEY `fk_specification_preset_items_specifications1_idx` (`specifications__id`);

--
-- Indexes for table `specification_values`
--
ALTER TABLE `specification_values`
  ADD PRIMARY KEY (`_id`),
  ADD KEY `fk_specification_values_specifications1_idx` (`specifications__id`);

--
-- Indexes for table `tax`
--
ALTER TABLE `tax`
  ADD PRIMARY KEY (`_id`),
  ADD KEY `fk_tax_countries1_idx` (`countries__id`);

--
-- Indexes for table `temp_emails`
--
ALTER TABLE `temp_emails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_temp_emails_users1_idx` (`users_id`);

--
-- Indexes for table `transliterates`
--
ALTER TABLE `transliterates`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`_id`);

--
-- Indexes for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD PRIMARY KEY (`_id`),
  ADD KEY `fk_wishlist_products1_idx` (`products_id`),
  ADD KEY `fk_wishlist_users1_idx` (`users_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_logs`
--
ALTER TABLE `activity_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `addresses`
--
ALTER TABLE `addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1000;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `coupon_products`
--
ALTER TABLE `coupon_products`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ordered_products`
--
ALTER TABLE `ordered_products`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ordered_product_options`
--
ALTER TABLE `ordered_product_options`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `order_logs`
--
ALTER TABLE `order_logs`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `order_payments`
--
ALTER TABLE `order_payments`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `order_taxes`
--
ALTER TABLE `order_taxes`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `password_resets`
--
ALTER TABLE `password_resets`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_notifications`
--
ALTER TABLE `product_notifications`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_option_labels`
--
ALTER TABLE `product_option_labels`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_option_values`
--
ALTER TABLE `product_option_values`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_questions`
--
ALTER TABLE `product_questions`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_ratings`
--
ALTER TABLE `product_ratings`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_specifications`
--
ALTER TABLE `product_specifications`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `related_products`
--
ALTER TABLE `related_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- AUTO_INCREMENT for table `shipping`
--
ALTER TABLE `shipping`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `shipping_types`
--
ALTER TABLE `shipping_types`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `social_user_registrations`
--
ALTER TABLE `social_user_registrations`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `specifications`
--
ALTER TABLE `specifications`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `specification_presets`
--
ALTER TABLE `specification_presets`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `specification_preset_items`
--
ALTER TABLE `specification_preset_items`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `specification_values`
--
ALTER TABLE `specification_values`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tax`
--
ALTER TABLE `tax`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temp_emails`
--
ALTER TABLE `temp_emails`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_roles`
--
ALTER TABLE `user_roles`
  MODIFY `_id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wishlist`
--
ALTER TABLE `wishlist`
  MODIFY `_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `addresses`
--
ALTER TABLE `addresses`
  ADD CONSTRAINT `fk_addresses_countries1` FOREIGN KEY (`countries__id`) REFERENCES `countries` (`_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_addresses_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `coupon_products`
--
ALTER TABLE `coupon_products`
  ADD CONSTRAINT `fk_coupon_products_coupons1` FOREIGN KEY (`coupons__id`) REFERENCES `coupons` (`_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_coupon_products_products1` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `ordered_products`
--
ALTER TABLE `ordered_products`
  ADD CONSTRAINT `fk_ordered_producs_orders1` FOREIGN KEY (`orders__id`) REFERENCES `orders` (`_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ordered_producs_products1` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `ordered_product_options`
--
ALTER TABLE `ordered_product_options`
  ADD CONSTRAINT `fk_ordered_product_options_ordered_products1` FOREIGN KEY (`ordered_products__id`) REFERENCES `ordered_products` (`_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `fk_orders_addresses1` FOREIGN KEY (`addresses_id`) REFERENCES `addresses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_orders_addresses2` FOREIGN KEY (`addresses_id1`) REFERENCES `addresses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_orders_coupons1` FOREIGN KEY (`coupons__id`) REFERENCES `coupons` (`_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_orders_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `order_logs`
--
ALTER TABLE `order_logs`
  ADD CONSTRAINT `fk_order_logs_orders1` FOREIGN KEY (`orders__id`) REFERENCES `orders` (`_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_logs_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `order_payments`
--
ALTER TABLE `order_payments`
  ADD CONSTRAINT `fk_order_payments_orders1` FOREIGN KEY (`orders__id`) REFERENCES `orders` (`_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `order_taxes`
--
ALTER TABLE `order_taxes`
  ADD CONSTRAINT `fk_order_taxes_orders1` FOREIGN KEY (`orders__id`) REFERENCES `orders` (`_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_taxes_tax1` FOREIGN KEY (`tax__id`) REFERENCES `tax` (`_id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `fk_products_brands1` FOREIGN KEY (`brands__id`) REFERENCES `brands` (`_id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_products_specification_presets1` FOREIGN KEY (`specification_presets__id`) REFERENCES `specification_presets` (`_id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD CONSTRAINT `fk_product_catgeories_categories1` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_catgeories_products1` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `fk_product_images_product_option_values1` FOREIGN KEY (`product_option_values_id`) REFERENCES `product_option_values` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_images_products1` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `product_notifications`
--
ALTER TABLE `product_notifications`
  ADD CONSTRAINT `fk_product_notifications_products1` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_notifications_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `product_option_labels`
--
ALTER TABLE `product_option_labels`
  ADD CONSTRAINT `fk_product_option_labels_products1` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `product_option_values`
--
ALTER TABLE `product_option_values`
  ADD CONSTRAINT `fk_product_option_values_product_option_labels1` FOREIGN KEY (`product_option_labels_id`) REFERENCES `product_option_labels` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `product_questions`
--
ALTER TABLE `product_questions`
  ADD CONSTRAINT `fk_faqs_products1` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_faqs_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_questions_product_questions1` FOREIGN KEY (`product_questions__id`) REFERENCES `product_questions` (`_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `product_ratings`
--
ALTER TABLE `product_ratings`
  ADD CONSTRAINT `fk_product_ratings_products1` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_ratings_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `product_specifications`
--
ALTER TABLE `product_specifications`
  ADD CONSTRAINT `fk_product_specifications_products1` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_specifications_specification_values1` FOREIGN KEY (`specification_values__id`) REFERENCES `specification_values` (`_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_specifications_specifications1` FOREIGN KEY (`specifications__id`) REFERENCES `specifications` (`_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `related_products`
--
ALTER TABLE `related_products`
  ADD CONSTRAINT `fk_related_products_products1` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_related_products_products2` FOREIGN KEY (`related_product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `shipping`
--
ALTER TABLE `shipping`
  ADD CONSTRAINT `fk_shipping_countries1` FOREIGN KEY (`countries__id`) REFERENCES `countries` (`_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_shipping_shipping_types1` FOREIGN KEY (`shipping_types__id`) REFERENCES `shipping_types` (`_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `social_user_registrations`
--
ALTER TABLE `social_user_registrations`
  ADD CONSTRAINT `fk_social_user_registrations_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `specification_preset_items`
--
ALTER TABLE `specification_preset_items`
  ADD CONSTRAINT `fk_specification_preset_items_specification_presets1` FOREIGN KEY (`specification_presets__id`) REFERENCES `specification_presets` (`_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_specification_preset_items_specifications1` FOREIGN KEY (`specifications__id`) REFERENCES `specifications` (`_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `specification_values`
--
ALTER TABLE `specification_values`
  ADD CONSTRAINT `fk_specification_values_specifications1` FOREIGN KEY (`specifications__id`) REFERENCES `specifications` (`_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `tax`
--
ALTER TABLE `tax`
  ADD CONSTRAINT `fk_tax_countries1` FOREIGN KEY (`countries__id`) REFERENCES `countries` (`_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `temp_emails`
--
ALTER TABLE `temp_emails`
  ADD CONSTRAINT `fk_temp_emails_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `wishlist`
--
ALTER TABLE `wishlist`
  ADD CONSTRAINT `fk_wishlist_products1` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_wishlist_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
