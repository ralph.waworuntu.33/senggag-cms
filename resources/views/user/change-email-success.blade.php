<div class="lw-custom-page-jumbotron jumbotron">
    <div class="lw-error-404">

	    <div class="lw-error-code m-b-10 m-t-20">
	    	<i class="fa fa-check-square-o fa-1x lw-success"></i> @section('page-title',  __tr(' Change Email' ))
	    </div>

    	<h2 class="font-bold"><?=  __tr("Aktifasi Akun Anda")  ?></h2>

	    <div class="fa-1x">
	        <?=  __tr('Hampir Selesai... Kami perlu konfirmasi alamat email anda. Untuk menyelesaikan proses aktivasi, Silakan klik tautan di email yang baru saja kami kirimkan kepada Anda ..')  ?>
	    </div>

	</div>
</div>