<div ng-controller="UserChangePasswordController as updatePasswordCtrl" class="col-lg-6">

    <div class="lw-section-heading-block">
        <!--  main heading -->
        <h3 class="lw-section-heading"><?=  __tr( 'Ganti Password' )  ?> @section('page-title', __tr('Change Password'))</h3>
        <!--  /main heading -->
    </div>

    @if(isLoggedIn())
        <div class="alert alert-info lw-row">
            <center><?= __tr('Jika Anda telah login / terdaftar melalui Akun Sosial (Google, Facebook, dll) & tidak mengatur ulang kata sandi Anda sebelum kemudian keluar dari akun Anda dan menggunakan fungsi kata sandi yang lupa untuk meresetnya. ') ?></center>
        </div>
    @endif

	<!--  form action -->
    <form class="lw-form lw-ng-form" 
        name="updatePasswordCtrl.[[ updatePasswordCtrl.ngFormName ]]" 
        ng-submit="updatePasswordCtrl.submit()" 
        novalidate>
        
        <!--  Current Password -->
        <lw-form-field field-for="current_password" label="<?=  __tr( 'Password Saat Ini' )  ?>"> 
            <input type="password" 
                  class="lw-form-field form-control"
                  name="current_password"
                  ng-minlength="6"
                  ng-maxlength="30"
                  ng-required="true" 
                  autofocus
                  ng-model="updatePasswordCtrl.userData.current_password" />
        </lw-form-field>
        <!--  /Current Password -->

        <!--  New Password -->
        <lw-form-field field-for="new_password" label="<?=  __tr( 'Password Baru' )  ?>"> 
            <input type="password" 
                  class="lw-form-field form-control"
                  name="new_password"
                  ng-minlength="6"
                  ng-maxlength="30"
                  ng-required="true" 
                  ng-model="updatePasswordCtrl.userData.new_password" />
        </lw-form-field>
        <!--  /New Password -->

        <!--  New Password Confirmation -->
        <lw-form-field field-for="new_password_confirmation" label="<?=  __tr( 'Konfirmasi Password Baru' )  ?>">
            <input type="password" 
                  class="lw-form-field form-control"
                  name="new_password_confirmation"
                  ng-minlength="6"
                  ng-maxlength="30"
                  ng-required="true" 
                  ng-model="updatePasswordCtrl.userData.new_password_confirmation" />
        </lw-form-field>
        <!--  /New Password Confirmation -->
		<br>
		<!--  update password button -->
        <div class="modal-footer">
            <button type="submit" class="lw-btn btn btn-primary" title="<?=  __tr('Update Password')  ?>"><?=  __tr('Ubah Password')  ?> <span></span></button>
        </div>
		<!--  /update password button -->

    </form>
	<!--  /form action -->
</div>